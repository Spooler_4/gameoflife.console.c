﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using GameOfLife;
using Newtonsoft.Json;

namespace GameOfLife
{
    public static class Globals
    {
        public static int Option1 = 1;
        public static int Option2 = 2;
        public static int Option3 = 3;
        public const char aliveCellChar = '\u2588';
        
        public const string aliveCellString = "\u2588";
    }
    public class Menu
    {




        public static void DisplayMenu()
        {
            /*Menu handling */

            string ConsoleInput;
            int MenuOutput = 0;
            int MenuInputCounter = 0;

            if (MenuInputCounter == 0)
            {
                Console.WriteLine("--- Game of Life ---");
                Console.WriteLine("1. Create Template");
                Console.WriteLine("2. Play Game");
                Console.WriteLine("3. Exit");
                Console.Write("Enter an option: ");
                ConsoleInput = Console.ReadLine();
                MenuInputCounter++;
                if (Int32.TryParse(ConsoleInput, out MenuOutput))
                {


                    /*Correct input has been entered into menu*/
                    ExecuteMenuOption(MenuOutput);




                }

                else
                {
                    InvalidInput();
                }

            }
            else
            {
                InvalidInput();
            }

        }


        //performs menu input validation
        public static void ExecuteMenuOption(int MenuOutput)
        {

            /*Correct input has been entered into menu*/
            /*Option execution block*/
            if ((MenuOutput == Globals.Option1) || (MenuOutput == Globals.Option2) || (MenuOutput == Globals.Option3))
            {

                if (MenuOutput == Globals.Option1)
                { TemplateInput(); }

                if (MenuOutput == Globals.Option2)
                { PlayGameMenu(); }

                if (MenuOutput == Globals.Option3)
                {
                    Console.WriteLine("Goodbye.");
                    System.Threading.Thread.Sleep(1000);
                    Environment.Exit(0);
                }



                /*end option execution block*/
                /*end menu handling*/

            }
            //if input in menu was numeric but not a number between 1 and 3
            else
            {
                InvalidInput();
            }
        }

        public static void TemplateInput()
        {

            string TemplateString = "";
            string XOInput = "";
            string Name = @"Templates\";
            string ConsoleInput;
            int Height = 0;
            int Width = 0;
            Regex XOxo = new Regex(@"^[XxOo]+$");



            Console.WriteLine("---Create Template---");


            Console.Write("Enter Name:");
            Name += Console.ReadLine();
            Name += ".txt";

            Console.Write("Enter Height: ");
            ConsoleInput = Console.ReadLine();
            if (Int32.TryParse(ConsoleInput, out Height))
            {
                TemplateString += Height.ToString();
                TemplateString += Environment.NewLine;
            }
            else { InvalidInput(); }
            if (Height <= 0) { InvalidInput(); }


            Console.Write("Enter Width: ", TemplateString);
            ConsoleInput = Console.ReadLine();
            if (Int32.TryParse(ConsoleInput, out Width))
            {
                TemplateString += Width.ToString();
                TemplateString += Environment.NewLine;
            }
            else { InvalidInput(); }
            if (Width <= 0) { InvalidInput(); }


            Console.WriteLine("Enter cells ('O' is alive, 'X' is Dead):");

            for (int InputCounter = 0; InputCounter < Height; InputCounter = InputCounter + 1)
            {
                XOInput = Console.ReadLine();
                if (XOxo.IsMatch(XOInput))
                {
                    if ((XOInput.Length == Width))
                    {
                        TemplateString += XOInput;
                        TemplateString += Environment.NewLine;
                    }


                    else
                    { InvalidInput(); }
                }


                else
                {

                    InvalidInput();
                }



            }
            System.IO.File.WriteAllText(Name, TemplateString);
            Console.WriteLine("Template Created.");
            DisplayMenu();

        }



 
        public static void PlayGameMenu()
        {
            Regex XOxo = new Regex(@"^[XxOo]+$"); Regex Numeric = new Regex("^[1-9][0-9]*$");
            string FolderPath = string.Format("{0}/{1}", Directory.GetCurrentDirectory(), "Templates");
            DirectoryInfo dir = new DirectoryInfo(FolderPath);
            string ConsoleInput;
            int InputValidation;
            int TemplateHeightInt;
            int TemplateWidthInt;
            string TemplateWidth, TemplateHeight;
            string ChooseTemplate = @"Templates\";
            string TemplateName = "";


            int Count;
            /*         Option selection for play game sub menu            */
            Console.WriteLine("--- Play Game ---");
            Console.WriteLine("1. New Game");
            Console.WriteLine("2. Resume Game");
            ConsoleInput = Console.ReadLine();


            if (Int32.TryParse(ConsoleInput, out InputValidation))
            {
                if (InputValidation == Globals.Option1)
                {
                    if (!dir.Exists)
                    {
                        dir.Create();
                    }
                    FileInfo[] Templates = dir.GetFiles("*.txt");
                    if (Templates.Length <= 0) { Console.WriteLine("There's no templates in this folder !"); InvalidInput(); } //if templates folder is empty call invalid input 
                    for (int i = 0; i < Templates.Length; i++)
                    {
                        string line = string.Format("{0}. {1}", i + 1, Templates[i].Name);
                        Console.WriteLine(line);
                    }
                    Console.WriteLine("Select a template:");
                    ConsoleInput = Console.ReadLine();
                    if (Int32.TryParse(ConsoleInput, out InputValidation))
                    {
                  
                        TemplateName = (Templates[InputValidation - 1].Name);
                        TemplateName = TemplateName.Replace(".txt", "");
           
                        ChooseTemplate += Templates[InputValidation - 1].Name;
                        Count = File.ReadLines(ChooseTemplate).Count();


                        if (Count > 0)
                        {
                            TemplateHeight = File.ReadLines(ChooseTemplate).Skip(0).Take(1).First();
                            TemplateWidth = File.ReadLines(ChooseTemplate).Skip(1).Take(1).First();
                            string[] Cells = File.ReadLines(ChooseTemplate).Skip(2).ToArray();

                            if (Numeric.IsMatch(TemplateHeight) && (Numeric.IsMatch(TemplateWidth))){ } else { InvalidInput(); }

                            Int32.TryParse(TemplateHeight, out TemplateHeightInt);
                            Int32.TryParse(TemplateWidth, out TemplateWidthInt);
                            char[][] JAState = new char[TemplateHeightInt][]; Cell[][] ToCell = new Cell[TemplateHeightInt][];

                            for (int j = 0; j < Count - 2; j++)
                            {
                                if (!XOxo.IsMatch(Cells[j])) { InvalidInput(); }
                          
                            }

                            for (int i = 0; i < TemplateHeightInt; i++)
                            {
                                JAState[i] = Cells[i].ToCharArray();        
                                                  
                            }

                            for (int i = 0; i < TemplateHeightInt; i++)
                            {
                                ToCell[i] = new Cell[TemplateWidthInt];
                                for (int a = 0; a < TemplateWidthInt; a++)
                                {
                                    ToCell[i][a] = Cell.Dead;
                                }
                           
                            }


                            for (int i = 0; i < JAState.Length; i++)
                            {
                                char[] innerArray = JAState[i];
                                for (int a = 0; a < innerArray.Length; a++)
                                {
                                   if ((innerArray[a] == 'x')||(innerArray[a] == 'X')) { JAState[i][a] = ' '; }
                                    if ((innerArray[a] == 'o') || (innerArray[a] == 'O')) { JAState[i][a] = Globals.aliveCellChar;}                                 
                                }                             
                            }
                            
                            for (int i = 0; i < JAState.Length; i++)
                            {


                                char[] innerArray = JAState[i];
                                Cell[] innerCell =  ToCell[i];                               
                                
                                for (int a = 0; a < innerArray.Length; a++)
                                {                                    
                                    if (innerArray[a] == ' ') { innerCell[a] = Cell.Dead; }                                 
                                    if (innerArray[a] == Globals.aliveCellChar) { innerCell[a] = Cell.Alive; }                                 

                                }
                               
                            }
                            
                            
                            Template template = new Template(TemplateName, TemplateHeightInt, TemplateWidthInt, ToCell);
                            template.DisplayToString();
                            
                          
                            

                            string GameHeight = "";
                            string GameWidth = "";
                            string GameX = "";
                            string GameY = "";
                            int GameHeightInt ;
                            int GameWidthInt ;
                            int GameXInt;
                            int GameYInt;

                            //Create Game world
                            Console.Write("Enter game height (must be atleast "); Console.Write(TemplateHeight); Console.Write("): "); GameHeight=Console.ReadLine();
                            if (Int32.TryParse(GameHeight, out GameHeightInt))
                                { if (GameHeightInt < TemplateHeightInt) { InvalidInput(); } } else { InvalidInput(); }
                           
                            Console.Write("Enter game Width (must be atleast "); Console.Write(TemplateWidth); Console.Write("): "); GameWidth=Console.ReadLine();
                            if (Int32.TryParse(GameWidth, out GameWidthInt))
                            { if (GameWidthInt < TemplateWidthInt) { InvalidInput(); } }
                            else { InvalidInput(); }
                            int maxX = (GameHeightInt - TemplateHeightInt);
                            int maxY = (GameWidthInt - TemplateWidthInt);
                            Console.Write("Enter Template X coordinate (Cannot be more than  "); Console.Write(maxX); Console.Write("): "); GameX = Console.ReadLine();
                            if (Int32.TryParse(GameX, out GameXInt))
                            { if (GameXInt > maxX) { InvalidInput(); } }
                            else { InvalidInput(); }
                            Console.Write("Enter Template Y coordinate (Cannot be more than "); Console.Write(maxY); Console.Write("): "); GameY = Console.ReadLine();
                            if (Int32.TryParse(GameY, out GameYInt))
                            { if (GameYInt > maxY) { InvalidInput(); } }
                            else { InvalidInput(); }
                            //pass game world nfo to game.

                            IGameOfLife gameOfLife = new GameOfLife(GameHeightInt, GameWidthInt);                            
                            gameOfLife.InsertTemplate(template, GameXInt, GameYInt);
                           
                            gameOfLife.PlayGame();






                        }

                    }



                }
                if (InputValidation == Globals.Option2)
                {
                    ResumeGame();    
                }
                else { InvalidInput(); }
            }
        }

      
        public static void ResumeGame()
        {
            string letsgetserialagain = File.ReadAllText("./json.txt");
            GameOfLife restoredSerial = JsonConvert.DeserializeObject<GameOfLife>(letsgetserialagain);
            restoredSerial.PlayGame();
        }


        public static void InvalidInput()
        { Console.WriteLine("InvalidInput"); DisplayMenu(); }

       



    }

}



