﻿using Newtonsoft.Json;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    public class GameOfLife : IGameOfLife
    {
        public GameOfLife(int height, int width)
        {

            Height = height;
            Width = width;
        }

        public int Height { get; set; }
        private string currentCell { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }

        public void InsertTemplate(ITemplate template, int templateX, int templateY)
        {
            Cell[][] Board = new Cell[Height][];
            int height = Height;
            int width = Width;

            for (int i = 0; i < Height; i++)
            {
                Board[i] = new Cell[Width];
                for (int a = 0; a < Width; a++)
                {
                    Board[i][a] = Cell.Dead;
                }

            }
            

            for (int i = 0; i < template.Height; i++)
            {


                Cell[] innerArray = template.Cells[i];
                for (int a = 0; a < template.Width; a++)
                {
                    if ((innerArray[a] == Cell.Dead)) { Board[templateY + i][templateX + a] = Cell.Dead; }

                    if ((innerArray[a] == Cell.Alive)) { Board[templateY + i][templateX + a] = Cell.Alive; }
                    


                }
                

            }

            Cells = Board;
            Height = height;
            Width = width;
            Console.ReadLine();
            ToString();
            

        }

        
        public void PlayGame()
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
            Task playGameTask = PlayGameAsync(cancellationTokenSource.Token);

          
            Console.ReadKey(true);

            cancellationTokenSource.Cancel();
            try
            {
                playGameTask.Wait(cancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            { }

            
            string letsgetserial = JsonConvert.SerializeObject(this);
            System.IO.File.WriteAllText("json.txt", letsgetserial);
            Console.WriteLine("Game stopped.");
            Menu.DisplayMenu();
        }

        public async Task PlayGameAsync(CancellationToken cancellationToken)
        {

            
            while (!cancellationToken.IsCancellationRequested)
            {

                Console.Clear();
                ToString();                
                Console.WriteLine("Press any key to stop game...");
                try
                {
                    
                    await Task.Delay(TimeSpan.FromSeconds(1), cancellationToken);
                    TakeTurn();

                }
                catch (TaskCanceledException)
                {
                }

            }
        }

        public void TakeTurn()
        {

            //Build Temporary array
            Cell[][] tempcells = new Cell[Height][];
            for (int i = 0; i < Height; i++)
            {
                tempcells[i] = new Cell[Width];
                for (int a = 0; a < Width; a++)
                {
                    tempcells[i][a] = Cell.Dead;
                }

            }

            for (int i = 0; i < Height; i++)
            {

                for (int j = 0; j < Width; j++)
                {
                    if ((Cells[i][j] == Cell.Dead)) { tempcells[i][j] = Cell.Dead; }
                    if ((Cells[i][j] == Cell.Alive)) { tempcells[i][j] = Cell.Alive; }
                }
            }
            
                //Temp array built and populated

                for (int y = 0; y<Height; y++)
            {
                Cell[] innerArray = tempcells[y];
                for (int x = 0; x < Width; x++)
                {
                    int aliveNeighbours = AliveNeighbours(x, y);
                    if (innerArray[x] == Cell.Dead)
                    {
                        if (aliveNeighbours == 3)
                        { tempcells[y][x] = Cell.Alive; }

                    }
                    if (innerArray[x] == Cell.Alive)
                    {
                        if (aliveNeighbours < 2)
                        { tempcells[y][x] = Cell.Dead; }
                        if (aliveNeighbours == 2)
                        { tempcells[y][x] = Cell.Alive; }
                        if (aliveNeighbours == 3)
                        { tempcells[y][x] = Cell.Alive; }
                        else if (aliveNeighbours > 3)
                        { tempcells[y][x] = Cell.Dead; }

                    }
                    
                    else
                    {
                        
                    }
                }
                
            }

            for (int i = 0; i < Height; i++)
            {

                for (int j = 0; j < Width; j++)
                {
                    if (tempcells[i][j] == Cell.Dead) { Cells[i][j] = Cell.Dead; }
                    if (tempcells[i][j] == Cell.Alive) { Cells[i][j] = Cell.Alive ; }
                }
            }
           // Cells = tempcells;




        }

        public int AliveNeighbours(int x, int y)
        {
            int sum = 0;

            for (int i = y -1; i <=y+1 ; i++)
            {
                for (int j = x - 1; j <= x + 1 ; j++)
                {
                    if(i<0||i>=Height||j<0||j>=Width||(i==y&&j==x))
                    { continue; }

                    if(Cells[i][j]==Cell.Alive)
                    { sum++; }
                }
                
            }


            return (sum);
        }


        public override string ToString()
        {
            string BoardtoString = "";
            

            char[][] Output = new char[Height][];

            for (int i = 0; i < Height; i++)
            {
                Output[i] = new char[Width];
                for (int a = 0; a < Width; a++)
                {
                    Output[i][a] = 'x';


                }

            }


            for (int i = 0; i < Height; i++)
            {

               
                Cell[] innerArray = Cells[i];
                for (int a = 0; a < innerArray.Length; a++)
                {
                    if ((innerArray[a] == Cell.Dead)) { Output[i][a] = ' '; }

                    if ((innerArray[a] == Cell.Alive)) { Output[i][a] = Globals.aliveCellChar; }
                    BoardtoString += Output[i][a];


                }
                BoardtoString+=Environment.NewLine;

            }

            Console.Write(BoardtoString);

            return BoardtoString.ToString();





            
            
        }

          
    }

 }

