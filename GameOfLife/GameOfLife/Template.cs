﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace GameOfLife
{
    public class Template : ITemplate
    {

      

        public Template(string name, int height, int width, Cell[][] cells)
        {
            Name = name;
            Height = height;
            Width = width;
            Cells = cells;
        }

        public string Name { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public Cell[][] Cells { get; set; }



        public string DisplayToString()
        {
            string TemplateToString = "";
            TemplateToString += "Template";
            TemplateToString += Environment.NewLine;
            TemplateToString += "Name: ";
            TemplateToString += Name;
            TemplateToString += Environment.NewLine;
            TemplateToString += "Height: ";
            TemplateToString += Height;
            TemplateToString += Environment.NewLine;
            TemplateToString += "Width: ";
            TemplateToString += Width;
            TemplateToString += Environment.NewLine;

            string[][] Output = new string[Height][];

            for (int i = 0; i < Height; i++)
            {
                Output[i] = new string[Width];
                for (int a = 0; a < Width; a++)
                {
                    Output[i][a] = "x";


                }

            }


            for (int i = 0; i < Height; i++)
            {


                Cell[] innerArray = Cells[i];
                for (int a = 0; a < innerArray.Length; a++)
                {
                    if ((innerArray[a] == Cell.Dead)) { Output[i][a] = "   "; }

                    if ((innerArray[a] == Cell.Alive)) { Output[i][a] = Globals.aliveCellString; }
                    TemplateToString += Output[i][a].ToString();


                }
                TemplateToString += Environment.NewLine;


            }

            Console.WriteLine(TemplateToString);
            return (TemplateToString);
        }
        public override string ToString()
        {
            //throw new NotImplementedException();

            string TemplateToString = "";
          
            
            
            TemplateToString += Height;
            TemplateToString += Environment.NewLine;          
            TemplateToString += Width;
            TemplateToString += Environment.NewLine;
            
           

            string[][] Output = new string[Height][];

            for (int i = 0; i < Height; i++)
            {
                Output[i] = new string[Width];
                for (int a = 0; a < Width; a++)
                {
                    Output[i][a] = "x";

              
                }
               
            }


            for (int i = 0; i < Height; i++)
                {


                    Cell[] innerArray = Cells[i];
                    for (int a = 0; a < innerArray.Length; a++)
                    {
                        if ((innerArray[a] == Cell.Dead) ) { Output[i][a] = "X"; }

                        if ((innerArray[a] == Cell.Alive) ) { Output[i][a] = "O"; }
                        TemplateToString += Output[i][a].ToString();
                        
                    
                }
                TemplateToString += Environment.NewLine;
                 
                
                }

            Console.WriteLine(TemplateToString);
            return (TemplateToString);


        }





    }
}
